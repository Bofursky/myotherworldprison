package net.myotherworld.Prison.Data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class TutorialData 
{
	public LinkedHashMap<ItemStack, Integer> items = new LinkedHashMap<ItemStack, Integer>();
	public LinkedHashMap<ItemStack, String> cmd = new LinkedHashMap<ItemStack, String>();
	
	public String Commands;
	public int Location;
	
	public TutorialData(YamlConfiguration Tutorial) 
	{	
		ConfigurationSection c = Tutorial.getConfigurationSection("");
		if (c == null)
		{
			Bukkit.getLogger().severe("Usun Tutorial.yml i przeladuj ponownie");
			return;
		}
		for (String key : c.getKeys(false))
		{
			Commands = Tutorial.getString(key + ".Commands");
			Location = Tutorial.getInt(key + ".Location");
			
			List<String> tempLore = Tutorial.getStringList(key + ".Lore");
			List<String> lore = new ArrayList<String>();
			for(String temp : tempLore)
			{
				lore.add(ChatColor.translateAlternateColorCodes('&', temp));
			}
			int ID = Tutorial.getInt(key + ".ID");
			int dur = Tutorial.getInt(key + ".Durability");
			int am = Tutorial.getInt(key + ".Amount");
			String name = Tutorial.getString(key + ".Name");
			
			
			@SuppressWarnings("deprecation")
			ItemStack item = new ItemStack(ID, am, (short) dur);
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
			if (lore != null)
			{
				itemMeta.setLore(lore);
			}
			item.setItemMeta(itemMeta);
			ItemStack i = new ItemStack(item);
			
			items.put(i, Location);
			cmd.put(i, Commands);
		}
	}
}
