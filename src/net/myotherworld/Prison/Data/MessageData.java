package net.myotherworld.Prison.Data;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class MessageData 
{
	public String Prefix;
	public String Permissions;
	public String LastRank;
	public String RankupLocal;
	public String RankupGlobal;
	public String RankupWrong;
	public String RankCost;
	public String NoCommands;
	public List<String> Admins = new ArrayList<String>();
	public String WelcomeMsg;
	
	
	public MessageData(YamlConfiguration messageData)
	{
		Prefix = ChatColor.translateAlternateColorCodes('&', messageData.getString("Prefix" , "Prison"));
		Permissions = ChatColor.translateAlternateColorCodes('&',messageData.getString("Message.Permissions", "You do not have permission!" ));
		
		LastRank = ChatColor.translateAlternateColorCodes('&',messageData.getString("Message.LastRank", "To jest ostatnia ranga" ));
		RankupLocal = ChatColor.translateAlternateColorCodes('&',messageData.getString("Message.RankupLocal", "Awansowales do nowej rangi " ));
		RankupGlobal = ChatColor.translateAlternateColorCodes('&',messageData.getString("Message.RankupGlobal", "Gracz %player% awansowal do nastepnej rangi" ));
		RankupWrong = ChatColor.translateAlternateColorCodes('&',messageData.getString("Message.RankupWrong", "Zle skonfigurowane rangi albo pusty ranks.yml" ));
		RankCost = ChatColor.translateAlternateColorCodes('&',messageData.getString("Message.RankCost", "Cena rang" ));
		NoCommands = ChatColor.translateAlternateColorCodes('&',messageData.getString("Message.NoCommands", "Brak takiej komendy wpisz /MowPrison" ));
		WelcomeMsg = ChatColor.translateAlternateColorCodes('&',messageData.getString("Message.WelcomeMsg", "Gracz %player% zostal wiezniem" ));
		
		Admins = messageData.getStringList("Admins");	
	}
}
