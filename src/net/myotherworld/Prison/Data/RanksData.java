package net.myotherworld.Prison.Data;

import java.util.LinkedHashMap;
import java.util.List;

import net.myotherworld.Prison.Prison;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

public class RanksData
{
	public Prison plugin;
	public int cost;
	public Boolean lastRank;
	public List<String> Message;
	public List<String> RankupCommands;
	public String NextRank;
	
	public LinkedHashMap<String, Boolean> Groups = new LinkedHashMap<String, Boolean>();
	public LinkedHashMap<String, Integer> Price = new LinkedHashMap<String, Integer>();
	public LinkedHashMap<String, List<String>> Cmd = new LinkedHashMap<String, List<String>>();
	public LinkedHashMap<String, List<String>> Msg = new LinkedHashMap<String, List<String>>();
	public LinkedHashMap<String, String> Next = new LinkedHashMap<String, String>();
	public LinkedHashMap<String, Integer> Ranks = new LinkedHashMap<String, Integer>();
	
	public RanksData(Prison plugin)
	{
		this.plugin = plugin;	
	}
	
	public RanksData(YamlConfiguration data) 
	{			
		ConfigurationSection c = data.getConfigurationSection("");
		if (c == null)
		{
			Bukkit.getLogger().severe("Usun Ranks.yml i przeladuj ponownie");
			return;
		}
		for (String key : c.getKeys(false))
		{
			lastRank = data.getBoolean(key + ".LastRank");
			cost = data.getInt(key + ".Cost");
			RankupCommands = data.getStringList(key + ".RankupCommands");		
			Message = data.getStringList(key + ".Message");											
			NextRank = data.getString(key + ".NextRank");
			
			Groups.put(key, lastRank);
			Price.put(key, cost);
			Cmd.put(key, RankupCommands);
			Msg.put(key, Message);
			Next.put(key, NextRank);
			Ranks.put(NextRank, cost);
		}
	}
}
