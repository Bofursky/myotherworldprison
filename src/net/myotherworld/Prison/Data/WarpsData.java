package net.myotherworld.Prison.Data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class WarpsData 
{
	public String Perm;
	public int ID;
	public int dur;
	public String name;
	public ArrayList<String> lore;
	public String Cmd;
	
	public LinkedHashMap<Integer, ItemStack> items = new LinkedHashMap<Integer, ItemStack>();
	public LinkedHashMap<ItemStack, String> cmd = new LinkedHashMap<ItemStack, String>();
	
	public WarpsData(YamlConfiguration Warps)
	{	
		ConfigurationSection c = Warps.getConfigurationSection("");
		if (c == null)
		{
			Bukkit.getLogger().severe("Usun Warps.yml i przeladuj ponownie");
			return;
		}
		for (String Slots : c.getKeys(false))
		{
			Cmd = Warps.getString(Slots + ".Commands");
			
			List<String> tempLore = Warps.getStringList(Slots + ".Item.Lore");	
			lore = new ArrayList<String>();
			for(String temp : tempLore)
			{
				lore.add(ChatColor.translateAlternateColorCodes('&', temp));
			}	
			name = Warps.getString(Slots + ".Item.Name");
			
			ID = Warps.getInt(Slots + ".Item.Unlocked.ID");
			dur = Warps.getInt(Slots + ".Item.Unlocked.Durability");			
			
			@SuppressWarnings("deprecation")
			ItemStack item = new ItemStack(ID, 1, (short) dur);
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
			if (lore != null)
			{				
				itemMeta.setLore(lore);
			}
			item.setItemMeta(itemMeta);
			ItemStack i = new ItemStack(item);
			
			
			items.put(Integer.valueOf(Slots), i);
			cmd.put(i, Cmd);
		}	
	}

}
