package net.myotherworld.Prison.Data;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigData
{
	public String TutorialNameMenu;
	public int TutorialSizeMenu;
	public String WarpsNameMenu;
	public int WarpsSizeMenu;
	public List<String> Commands = new ArrayList<String>();
	public boolean EnableWelcomeMsg;
	public boolean EnableDynamicScore;
	public boolean EnableScore;
	
	public ConfigData(YamlConfiguration configData) 
	{	
		TutorialNameMenu = ChatColor.translateAlternateColorCodes('&', configData.getString("Tutorial.NameMenu" , "Tutorial MyOtherWorld"));
		TutorialSizeMenu = configData.getInt("Tutorial.SizeMenu" , 18);
		
		WarpsNameMenu = ChatColor.translateAlternateColorCodes('&', configData.getString("Warps.NameMenu" , "Warpy MyOtherWorld"));
		WarpsSizeMenu = configData.getInt("Warps.SizeMenu" , 36);
		
		EnableWelcomeMsg = configData.getBoolean("Global.EnableWelcomeMsg", true);
				
		Commands = configData.getStringList("Commands");
		
		EnableDynamicScore = configData.getBoolean("Scoreboards.Dynamic", false);
		EnableScore = configData.getBoolean("Scoreboards.Enable", true);
	}
}
