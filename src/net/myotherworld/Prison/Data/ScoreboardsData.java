package net.myotherworld.Prison.Data;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class ScoreboardsData 
{
	public String DynamicName;
	public int DynamicRefresh;
	public List<String> DynamicDisplay = new ArrayList<String>();
	public String StaticName;
	public List<String> StaticDisplay = new ArrayList<String>();

	public ScoreboardsData(YamlConfiguration scoreboards) 
	{	
		DynamicName = ChatColor.translateAlternateColorCodes('&', scoreboards.getString("Dynamic.Name" , "Name"));
		DynamicRefresh = scoreboards.getInt("Dynamic.Refresh", 60);
		DynamicDisplay = scoreboards.getStringList("Dynamic.Display");	
		
		StaticName = ChatColor.translateAlternateColorCodes('&', scoreboards.getString("Static.Name" , "Name"));
		StaticDisplay = scoreboards.getStringList("Static.Display");	
	}
}
