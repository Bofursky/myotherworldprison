package net.myotherworld.Prison;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class ScoreboardScheduler extends BukkitRunnable 
{
	private Prison plugin;

	public ScoreboardScheduler(Prison plugin)
	{
		this.plugin = plugin;
	}
	@Override
	public void run() 
	{		
		if(plugin.configData.EnableDynamicScore == true)
		{
			if(plugin.configData.EnableScore == true)
			{
				for(final Player player : Bukkit.getOnlinePlayers())
				{	
					Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable() 
					{
						public void run()
						{		 
							plugin.scoreboard.updateScoreboard(player);
						}
					});
				}
			}
		}
	}
}
