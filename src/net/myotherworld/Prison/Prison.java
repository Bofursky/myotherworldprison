package net.myotherworld.Prison;

import net.myotherworld.Prison.Commands.AdminCommand;
import net.myotherworld.Prison.Commands.RanksCommand;
import net.myotherworld.Prison.Commands.RankupCommand;
import net.myotherworld.Prison.Commands.TutorialCommand;
import net.myotherworld.Prison.Data.ConfigData;
import net.myotherworld.Prison.Data.MessageData;
import net.myotherworld.Prison.Data.RanksData;
import net.myotherworld.Prison.Data.ScoreboardsData;
import net.myotherworld.Prison.Data.TutorialData;
import net.myotherworld.Prison.Data.WarpsData;
import net.myotherworld.Prison.Listeners.PlayerListener;
import net.myotherworld.Prison.Managers.FileManager;
import net.myotherworld.Prison.Managers.ScoreboardManager;
import net.myotherworld.Prison.Managers.TutorialManager;
import net.myotherworld.Prison.Managers.VaultManager;
import net.myotherworld.Prison.Managers.WarpManager;
import net.myotherworld.Prison.TitleManagers.ActionbarManager;
import net.myotherworld.Prison.TitleManagers.TabManager;
import net.myotherworld.Prison.TitleManagers.TitlesManager;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Prison extends JavaPlugin
{
	public FileManager fileManager;
	public TutorialManager tutorialManager;
	public WarpManager warpsManager;
	public ScoreboardManager scoreboard;
	public TutorialData tutorial;
	public ScoreboardsData scoreboards;
	public RanksData ranks;
	public WarpsData warps;
	
	public TitlesManager titlesManager;
	public VaultManager vaultManager;
	public TabManager tabManager;
	public ActionbarManager actionbarManager;
	public MessageData messageData;
	public ConfigData configData;
	
	@Override
    public void onEnable()
    {	
		enableSettings();
		enableListeners();				
		enableCommands();
    }
	public void enableCommands()
	{
		getCommand("Rankup").setExecutor(new RankupCommand(this));
		getCommand("Ranks").setExecutor(new RanksCommand(this));
		getCommand("Tutorial").setExecutor(new TutorialCommand(this));
		getCommand("MowPrison").setExecutor(new AdminCommand(this));
	}
	
	public void enableListeners() 
    {
    	PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new PlayerListener(this), this);      
    }  
	
	@SuppressWarnings("deprecation")
	public void enableSettings()
	{
		//Menagers
		vaultManager = new VaultManager();
		tabManager = new TabManager();
		fileManager = new FileManager(this);
		
		tutorialManager = new TutorialManager(this);
		warpsManager = new WarpManager(this);
			
		//Titles
		titlesManager = new TitlesManager();
		actionbarManager = new ActionbarManager();
		
		//Config
		configData = new ConfigData(fileManager.loadConfig());
		
		//Messages
		messageData = new MessageData(fileManager.loadMessages());
		
		//Ranks
		ranks = new RanksData(fileManager.loadRanks());
		
		//Scoreboards
		scoreboards = new ScoreboardsData(fileManager.loadScoreboards());
		
		//Warps
		warps = new WarpsData(fileManager.loadWarps());	
		
		//tutorial
		tutorial = new TutorialData(fileManager.loadTutorial());	
				
		//Scoreboard
        scoreboard = new ScoreboardManager(this);      
        		
		
        int Ticks = this.scoreboards.DynamicRefresh * 20;
        getServer().getScheduler().runTaskTimer(this, new ScoreboardScheduler(this), Ticks, Ticks);
	}
}