package net.myotherworld.Prison.TitleManagers;

import org.bukkit.entity.Player;

import io.puharesource.mc.titlemanager.api.ActionbarTitleObject;

public class ActionbarManager
{
	public void sendActionbar(Player player, String title)
	{
		ActionbarTitleObject actionbarTitleObject = new ActionbarTitleObject(title);
		actionbarTitleObject.broadcast();
	}
}
