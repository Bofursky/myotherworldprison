package net.myotherworld.Prison.TitleManagers;

import io.puharesource.mc.titlemanager.api.TitleObject;
import io.puharesource.mc.titlemanager.api.TitleObject.TitleType;

import org.bukkit.entity.Player;

public class TitlesManager
{
	public void sendTitle(Player player, String title)
	{
		TitleObject titleObject = new TitleObject(title, TitleType.TITLE);
		titleObject.send(player);
	}
	
	public void sendTitle(Player player, String title, String subtitle)
	{
		TitleObject titleObject = new TitleObject(title, subtitle);
		titleObject.send(player);
	}
	
	public void sendTitle(String title)
	{
		TitleObject titleObject = new TitleObject(title, TitleType.TITLE);
		titleObject.broadcast();
	}
	
	public void sendTitle(String title, String subtitle)
	{
		TitleObject titleObject = new TitleObject(title, subtitle);
		titleObject.broadcast();
	}
	
}
