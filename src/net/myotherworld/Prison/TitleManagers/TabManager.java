package net.myotherworld.Prison.TitleManagers;

import io.puharesource.mc.titlemanager.api.TabTitleObject;

import org.bukkit.entity.Player;

public class TabManager 
{
	public void sendTab(Player player, String header, String footer)
	{
		TabTitleObject tabTitleObject = new TabTitleObject(header, footer);
		tabTitleObject.send(player);
	}
}
