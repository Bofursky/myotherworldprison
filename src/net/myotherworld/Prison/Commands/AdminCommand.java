package net.myotherworld.Prison.Commands;

import net.myotherworld.Prison.Prison;
import net.myotherworld.Prison.Data.ConfigData;
import net.myotherworld.Prison.Data.MessageData;
import net.myotherworld.Prison.Data.RanksData;
import net.myotherworld.Prison.Data.ScoreboardsData;
import net.myotherworld.Prison.Data.TutorialData;
import net.myotherworld.Prison.Data.WarpsData;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class AdminCommand implements CommandExecutor
{
	private Prison plugin;
	public AdminCommand(Prison plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(sender.hasPermission("MyOtherWorldPrison.admin")) 
		{		
			if (args.length > 0) 				
			{			
				if(args[0].equalsIgnoreCase("Reload"))
				{	    
					if (args.length == 2) 				
					{
						if(args[1].equalsIgnoreCase("Config"))
						{
							if(sender.hasPermission("MyOtherWorldPrison.admin.reload.config")) 
							{
								plugin.configData = new ConfigData(plugin.fileManager.loadConfig());
						        sender.sendMessage("Poprawnie przeladowano config.yml.");
						        return true;
							}
						}
						else if(args[1].equalsIgnoreCase("Msg"))
						{
							if(sender.hasPermission("MyOtherWorldPrison.admin.reload.message")) 
							{
								plugin.messageData = new MessageData(plugin.fileManager.loadMessages());
						        sender.sendMessage("Poprawnie przeladowano message.yml.");
						        return true;
							}	
						}
						else if(args[1].equalsIgnoreCase("Ranks"))
						{
							if(sender.hasPermission("MyOtherWorldPrison.admin.reload.ranks")) 
							{
								plugin.ranks = new RanksData(plugin.fileManager.loadRanks());
						        sender.sendMessage("Poprawnie przeladowano Ranks.yml.");
						        return true;
							}
						}
						else if(args[1].equalsIgnoreCase("Score"))
						{
							if(sender.hasPermission("MyOtherWorldPrison.admin.reload.scoreboards")) 
							{
								plugin.scoreboards = new ScoreboardsData(plugin.fileManager.loadScoreboards());
						        sender.sendMessage("Poprawnie przeladowano Scoreboards.yml.");
						        return true;
							}
						}
						else if(args[1].equalsIgnoreCase("Tutorial"))
						{
							if(sender.hasPermission("MyOtherWorldPrison.admin.reload.tutorial")) 
							{
								plugin.tutorial = new TutorialData(plugin.fileManager.loadTutorial());
						        sender.sendMessage("Poprawnie przeladowano Tutorial.yml.");
						        return true;
							}
						}
						else if(args[1].equalsIgnoreCase("Warps"))
						{
							if(sender.hasPermission("MyOtherWorldPrison.admin.reload.warps")) 
							{
								plugin.warps = new WarpsData(plugin.fileManager.loadWarps());
						        sender.sendMessage("Poprawnie przeladowano Warps.yml.");
						        return true;
							}
						}
						else if(args[1].equalsIgnoreCase("all"))
						{
							if(sender.hasPermission("MyOtherWorldPrison.admin.reload.all")) 
							{
								plugin.configData = new ConfigData(plugin.fileManager.loadConfig());
								plugin.messageData = new MessageData(plugin.fileManager.loadMessages());
								plugin.ranks = new RanksData(plugin.fileManager.loadRanks());
								plugin.warps = new WarpsData(plugin.fileManager.loadWarps());
								plugin.tutorial = new TutorialData(plugin.fileManager.loadTutorial());
								plugin.scoreboards = new ScoreboardsData(plugin.fileManager.loadScoreboards());
								sender.sendMessage("Poprawnie przeladowano wszystkie Cfg.");
								return true;
							}
						}
						else
						{
							sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.NoCommands);
							return true;
						}	
					}
					for(String m : plugin.messageData.Admins) 
					{
						m = ChatColor.translateAlternateColorCodes('&', m);
						sender.sendMessage(plugin.messageData.Prefix + m);						
					}
					return true;
				}
				else
				{
					for(String m : plugin.messageData.Admins) 
					{
						m = ChatColor.translateAlternateColorCodes('&', m);
						sender.sendMessage(plugin.messageData.Prefix + m);					
					}
					return true;
				}
			}
			else
			{
				for(String m : plugin.messageData.Admins) 
				{
					m = ChatColor.translateAlternateColorCodes('&', m);
					sender.sendMessage(plugin.messageData.Prefix + m);				
				}	
				return true;
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}
