package net.myotherworld.Prison.Commands;

import java.util.List;
import java.util.Map.Entry;

import net.myotherworld.Prison.Prison;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RankupCommand implements CommandExecutor
{	
	private Prison plugin;
	public RankupCommand(Prison plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
    	if(sender.hasPermission("MyOtherWorldPrison.rankup")) 
    	{	
    		Player player = (Player) sender;
    		if (args.length == 0) 
    		{
    			for (Entry<String, Boolean> Groups : plugin.ranks.Groups.entrySet())//Pobieranie grup z cfg
    			{
    				if(plugin.vaultManager.permission.playerInGroup(player, (String)Groups.getKey()))//Sprawdzanie czy gracz jest w danej grupie
    				{
    					if((Boolean)Groups.getValue() != true)//Sprawdzanie czy nie jest to ostatnia ranga
    					{
    						for (Entry<String, Integer> Price : plugin.ranks.Price.entrySet())//Pobieranie grup z cfg
    						{ 							
    							if(plugin.vaultManager.permission.playerInGroup(player, (String)Price.getKey()))//Sprawdzanie czy gracz jest w danej grupie
    							{
    								if(plugin.vaultManager.economy.has(player, (Integer)Price.getValue()))//Sprawdzanie czy gracz ma wystarczajaco gotowki
    								{
    									plugin.vaultManager.economy.withdrawPlayer(player, (Integer)Price.getValue());//Pobiera pieniadze od gracza
    									
    									for (Entry<String, List<String>> Cmd : plugin.ranks.Cmd.entrySet())
    									{
    										if(plugin.vaultManager.permission.playerInGroup(player, (String)Cmd.getKey()))//Sprawdzanie czy gracz jest w danej grupie
    			    						{ 
    											List<String> list = Cmd.getValue();//Lista komend 											
    											for(String commands : list)
    											{
    												commands = commands.replace("%player%", player.getName());//Zamianana gracza
    												Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), commands);//Wykonanie komendy												 												
    											}  	
    											for(Entry<String, String> Next : plugin.ranks.Next.entrySet())//Pobieranie grup z cfg
    											{
    							    				if(plugin.vaultManager.permission.playerInGroup(player, (String)Next.getKey()))//Sprawdzanie czy gracz jest w danej grupie
    							    				{
    							    					plugin.titlesManager.sendTitle(player, plugin.messageData.RankupLocal + Next.getKey());
    							    					if(plugin.configData.EnableScore == true)
    							    					{
    							    						plugin.scoreboard.updateScoreboard(player);
    							    					}
    							    					for (Player p : Bukkit.getOnlinePlayers()) 
    							    					{
    							    						String Msg = plugin.messageData.RankupGlobal;
    							    						Msg = Msg.replaceAll("%player%", player.getName());
    							    						plugin.actionbarManager.sendActionbar(p, Msg + Next.getKey());
        							    				
        							    				}  							    				
    							    					return true;
    							    				}
    											}
    			    						}
    									}   									
    								}
    								else//Wyswietlanie wiadomosci gdy gracz nie ma wystarczajaco gotowki
    								{
    									for (Entry<String, List<String>> Msg : plugin.ranks.Msg.entrySet())
    									{
											if(plugin.vaultManager.permission.playerInGroup(player, (String)Msg.getKey()))//Sprawdzanie czy gracz jest w danej grupie
				    						{ 
												List<String> list = Msg.getValue();//Lista Wiadomosci
												for(String commands : list)
												{
													commands = commands.replace("@p", player.getName());//Zamiana @p na gracza
													commands = commands.replace("%player%", player.getName());//Zamianana gracza
													Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), commands);//Wykonanie komendy
												}
												return true;
				    						}
    									}
    								}
    							}
    						}			
    					}
    					else
    					{	
    						sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.LastRank);
    						return true;
    					}					
    				}
    			}
    			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.RankupWrong);
				return true;
    		}
    	}	
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}	    	
		return true;		
	}
}
