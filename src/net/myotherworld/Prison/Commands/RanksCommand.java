package net.myotherworld.Prison.Commands;

import java.util.Map.Entry;

import net.myotherworld.Prison.Prison;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class RanksCommand implements CommandExecutor 
{
	private Prison plugin;
	public RanksCommand(Prison plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
    	if(sender.hasPermission("MyOtherWorldPrison.ranks")) 
    	{	
    		if (args.length == 0) 
    		{
    			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.RankCost);
    			for (Entry<String, Integer> Price : plugin.ranks.Ranks.entrySet())
    			{    				
    				sender.sendMessage(String.valueOf(Price.getKey() + ": " + Price.getValue()));
    			}
    		}
    		else
    		{
    			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
    		}	    	
    		return true;		
    	}
		return true;
	}
}
