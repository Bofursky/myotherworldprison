package net.myotherworld.Prison.Listeners;

import java.util.Map.Entry;

import net.myotherworld.Prison.Prison;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener
{
	private Prison plugin;
	public PlayerListener(Prison plugin)
	{
		this.plugin = plugin;
	}	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event)
	{
	    boolean IsCmd = false;
	    for (String Cmd : plugin.configData.Commands) 
	    {
	    	if (event.getMessage().equalsIgnoreCase("/" + Cmd)) 
	    	{
	    		IsCmd = true;
	    	}
	    }
	    if (!IsCmd) 
	    {
	    	return;
	    }
	    event.setCancelled(true);
	    Player player = event.getPlayer();
		plugin.warpsManager.loadWarpInv(player);	
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{	
		Player player = event.getPlayer();
		if(player.hasPlayedBefore() == !plugin.configData.EnableWelcomeMsg)
		{
			String Msg = plugin.messageData.WelcomeMsg;
			Msg = Msg.replaceAll("%player%", player.getName());
			Bukkit.broadcastMessage(Msg);
		}
		if(plugin.configData.EnableScore == true)
		{
			plugin.scoreboard.updateScoreboard(event.getPlayer());
		}
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{	
		if(plugin.configData.EnableScore == true)
		{
			Player player = event.getPlayer();
			plugin.scoreboard.removeScoreboard(player);
		}
	}
	
	@EventHandler
	public void interactInfo(InventoryClickEvent event)
	{
    	if (event.getInventory() == null)
    	{
    		return;
    	}
    	if (event.getInventory().getName().equalsIgnoreCase(plugin.configData.TutorialNameMenu))
    	{
    		event.setCancelled(true);
    		try
    		{ 		
    			for(Entry<ItemStack, String> Cmd : plugin.tutorial.cmd.entrySet())
    			{
        			if (event.getCurrentItem().equals(Cmd.getKey()) && Cmd.getValue() != null && !Cmd.getValue().equalsIgnoreCase("Exit"))
        	        {	
        				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), Cmd.getValue());
        	        }
        			if (event.getCurrentItem().equals(Cmd.getKey()) && Cmd.getValue().equalsIgnoreCase("Exit"))
        	        {	
        				Player p = (Player) event.getWhoClicked();
        				p.closeInventory();
        	        }
    				
    			}
    			
    		}
    	    catch (Exception localException) {}
    	}
    	if (event.getInventory().getName().equalsIgnoreCase(plugin.configData.WarpsNameMenu))
    	{
    		event.setCancelled(true);
    		try
    		{ 		
    			for(Entry<ItemStack, String> Cmd : plugin.warps.cmd.entrySet())
    			{
        			if (event.getCurrentItem().equals(Cmd.getKey()))
        	        {	
        				Player p = (Player) event.getWhoClicked();
        				p.performCommand(Cmd.getValue());
        	        }				
    			}
    			
    		}
    	    catch (Exception localException) {}
    	}
	}
}
