package net.myotherworld.Prison.Managers;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

public class VaultManager
{
  public Permission permission = null;
  public Economy economy = null;
  public Chat chat = null;
  
  public VaultManager()
  {
    setupPermissions();
    setupChat();
    setupEconomy();
  }
  
  private boolean setupPermissions()
  {
    RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServer().getServicesManager().getRegistration(Permission.class);
    if (permissionProvider != null) {
      this.permission = ((Permission)permissionProvider.getProvider());
    }
    return this.permission != null;
  }
  
  private boolean setupChat()
  {
    RegisteredServiceProvider<Chat> chatProvider = Bukkit.getServer().getServicesManager().getRegistration(Chat.class);
    if (chatProvider != null) {
      this.chat = ((Chat)chatProvider.getProvider());
    }
    return this.chat != null;
  }
  
  private boolean setupEconomy()
  {
    RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
    if (economyProvider != null) {
      this.economy = ((Economy)economyProvider.getProvider());
    }
    return this.economy != null;
  }
}
