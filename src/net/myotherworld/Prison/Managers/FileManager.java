package net.myotherworld.Prison.Managers;

import java.io.File;

import org.bukkit.configuration.file.YamlConfiguration;

import net.myotherworld.Prison.Prison;

public class FileManager 
{
	  private Prison plugin;

	  public FileManager(Prison plugin)
	  {
		  this.plugin = plugin;

	  }
	  
		public YamlConfiguration loadConfig()
		{
			try 
	        {
	            if (!plugin.getDataFolder().exists())
	            {
	            	plugin.getDataFolder().mkdirs();
	            }
	            if (!new File(plugin.getDataFolder(), "config.yml").exists())
	            {
	            	plugin.saveResource("config.yml", false);
	            }
	            return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "config.yml"));
	            
	        } 
			catch (Exception ex) 
	        {
	            plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	            plugin.getServer().getPluginManager().disablePlugin(plugin);
	            return null;
	        }
		}
		
		public YamlConfiguration loadMessages()
		{
			try 
	        {
	            if (!plugin.getDataFolder().exists())
	            {
	            	plugin.getDataFolder().mkdirs();
	            }
	            if (!new File(plugin.getDataFolder(), "messages.yml").exists())
	            {
	            	plugin.saveResource("messages.yml", false);
	            }
	            return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "messages.yml"));
	            
	        } 
			catch (Exception ex) 
	        {
	            plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	            plugin.getServer().getPluginManager().disablePlugin(plugin);
	            return null;
	        }
		}
		
		public YamlConfiguration loadRanks()
		{
			try 
	        {
	            if (!plugin.getDataFolder().exists())
	            {
	            	plugin.getDataFolder().mkdirs();
	            }
	            if (!new File(plugin.getDataFolder(), "ranks.yml").exists())
	            {
	            	plugin.saveResource("ranks.yml", false);
	            }
	            return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "ranks.yml"));
	            
	        } 
			catch (Exception ex) 
	        {
	            plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	            plugin.getServer().getPluginManager().disablePlugin(plugin);
	            return null;
	        }
		}
		public YamlConfiguration loadScoreboards()
		{
			try 
	        {
	            if (!plugin.getDataFolder().exists())
	            {
	            	plugin.getDataFolder().mkdirs();
	            }
	            if (!new File(plugin.getDataFolder(), "scoreboards.yml").exists())
	            {
	            	plugin.saveResource("scoreboards.yml", false);
	            }
	            return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "scoreboards.yml"));
	            
	        } 
			catch (Exception ex) 
	        {
	            plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	            plugin.getServer().getPluginManager().disablePlugin(plugin);
	            return null;
	        }
		}		 
		public YamlConfiguration loadWarps()
		{
			try 
	        {
	            if (!plugin.getDataFolder().exists())
	            {
	            	plugin.getDataFolder().mkdirs();
	            }
	            if (!new File(plugin.getDataFolder(), "warps.yml").exists())
	            {
	            	plugin.saveResource("warps.yml", false);
	            }
	            return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "warps.yml"));
	            
	        } 
			catch (Exception ex) 
	        {
	            plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	            plugin.getServer().getPluginManager().disablePlugin(plugin);
	            return null;
	        }
		}
		public YamlConfiguration loadTutorial()
		{
			try 
	        {
	            if (!plugin.getDataFolder().exists())
	            {
	            	plugin.getDataFolder().mkdirs();
	            }
	            if (!new File(plugin.getDataFolder(), "tutorial.yml").exists())
	            {
	            	plugin.saveResource("tutorial.yml", false);
	            }
	            return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "tutorial.yml"));
	            
	        } 
			catch (Exception ex) 
	        {
	            plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	            plugin.getServer().getPluginManager().disablePlugin(plugin);
	            return null;
	        }
		}	
}
