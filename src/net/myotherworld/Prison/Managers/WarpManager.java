package net.myotherworld.Prison.Managers;

import java.util.Map.Entry;

import net.myotherworld.Prison.Prison;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class WarpManager 
{	
	private Prison plugin;
	public WarpManager(Prison plugin)
	{
		this.plugin = plugin;
	}	
	public void loadWarpInv(Player p)
	{
		Inventory inv = Bukkit.createInventory(null, plugin.configData.WarpsSizeMenu, plugin.configData.WarpsNameMenu);
		
		p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 100.0F, 100.0F);

		for(Entry<Integer, ItemStack> Items : plugin.warps.items.entrySet())
		{    			
			inv.setItem(Items.getKey(), Items.getValue());
		}				
		p.openInventory(inv);
		return;
	}
}
