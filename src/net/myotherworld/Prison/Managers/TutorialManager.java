package net.myotherworld.Prison.Managers;

import java.util.Map.Entry;

import net.myotherworld.Prison.Prison;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class TutorialManager
{
	private Prison plugin;
	public TutorialManager(Prison plugin)
	{
		this.plugin = plugin;
	}	
	public void loadTutorialInv(Player p)
	{
		Inventory inv = Bukkit.createInventory(null, plugin.configData.TutorialSizeMenu, plugin.configData.TutorialNameMenu);
		
		p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 100.0F, 100.0F);
		
		for(Entry<ItemStack, Integer> Inv : plugin.tutorial.items.entrySet())
		{
			int Loc = Inv.getValue()-1;
			inv.setItem(Loc, Inv.getKey());
			
		}		
	    p.openInventory(inv);
	}
}
