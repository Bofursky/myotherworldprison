package net.myotherworld.Prison.Managers;

import java.util.HashMap;
import java.util.Map.Entry;

import net.myotherworld.Prison.Prison;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardManager
{
	private HashMap<String, Scoreboard> playerScoreboard = new HashMap<String, Scoreboard>();
	private Prison plugin;
	private Scoreboard scoreboard;
	private Objective objective;
	private Score score;
	
	public ScoreboardManager(Prison plugin)
	{
		this.plugin = plugin;
	}
	//Konstruktor dostepowy do scoreboards
	public Scoreboard getScoreboard()
	{
	  return this.scoreboard;
	}
	//Czyszczenie i wyrejestrowanie scoreboards dla graczy
	public void removeScoreboard(Player player)
	{
		playerScoreboard.remove(player.getName());
		player.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
        if(player.getScoreboard().getObjective(DisplaySlot.SIDEBAR) != null) 
        {
        	player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).unregister();    
        }    
	}
	//Ustawnienie nazwy scoreboards
	public void setTitle(String title)
	{
		this.objective.setDisplayName(title);
	}
	//Przygotowanie scoreboards
	public Scoreboard prepareScoreboard(Player player)
	{
		if(playerScoreboard.containsKey(player.getName()))
		{
			scoreboard = playerScoreboard.get(player.getName());
			objective = scoreboard.getObjective("Prison");
			objective.unregister();
			player.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
			objective = scoreboard.registerNewObjective("Prison" , "dummy");
			objective.setDisplaySlot(DisplaySlot.SIDEBAR);	
			if(plugin.configData.EnableDynamicScore == true)
			{
				objective.setDisplayName(plugin.scoreboards.DynamicName);
			}
			else
			{
				objective.setDisplayName(plugin.scoreboards.StaticName);
			}	
		}
		else
		{
			//Tworzymy scoreboard
			scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
			objective = scoreboard.registerNewObjective("Prison" , "dummy");
			objective.setDisplaySlot(DisplaySlot.SIDEBAR);	
			if(plugin.configData.EnableDynamicScore == true)
			{
				objective.setDisplayName(plugin.scoreboards.DynamicName);
			}
			else
			{
				objective.setDisplayName(plugin.scoreboards.StaticName);
			}
		}
		String line = null;
		if(plugin.configData.EnableDynamicScore == true)
		{
			varibleScoreboardDynamic(player, line);
		}
		else
		{
			varibleScoreboardStatic(player, line);
		}
		return scoreboard;
	}
	
	public void varibleScoreboardDynamic(Player player, String string)
	{
		this.setTitle(plugin.scoreboards.DynamicName);
		int pos = 0;
		
		for(String line : plugin.scoreboards.DynamicDisplay)
		{
			line = ChatColor.translateAlternateColorCodes('&', line);
	      
	        line = line.replaceAll("%displayname%", player.getDisplayName());    
	        
			for (Entry<String, Boolean> Rank : plugin.ranks.Groups.entrySet())
			{
				if(plugin.vaultManager.permission.playerInGroup(player, (String)Rank.getKey()))
				{
					line = line.replaceAll("%rank%", Rank.getKey());  
				}
			}
			for(Entry<String, String> Next : plugin.ranks.Next.entrySet())			
			{
				if(plugin.vaultManager.permission.playerInGroup(player, (String)Next.getKey()))
				{
					line = line.replaceAll("%nextrank%", Next.getValue());  
				}
			}
			for(Entry<String, Integer> Cost : plugin.ranks.Price.entrySet())
			{
				if(plugin.vaultManager.permission.playerInGroup(player, (String)Cost.getKey()))
				{
					line = line.replaceAll("%cost%", String.valueOf(Cost.getValue()));  
				}
			}
			Double money = plugin.vaultManager.economy.getBalance(player);
			line = line.replaceAll("%balance%", String.valueOf(money));
			
			score = objective.getScore(line);
			score.setScore(pos);
		    pos--;
		}
	}
	public void varibleScoreboardStatic(Player player, String string)
	{
		this.setTitle(plugin.scoreboards.StaticName);
		int pos = 0;
		
		for(String line : plugin.scoreboards.StaticDisplay)
		{
			line = ChatColor.translateAlternateColorCodes('&', line);
	      
	        line = line.replaceAll("%displayname%", player.getDisplayName());    
	        
			for (Entry<String, Boolean> Rank : plugin.ranks.Groups.entrySet())
			{
				if(plugin.vaultManager.permission.playerInGroup(player, (String)Rank.getKey()))
				{
					line = line.replaceAll("%rank%", Rank.getKey());  
				}
			}
			for(Entry<String, String> Next : plugin.ranks.Next.entrySet())			
			{
				if(plugin.vaultManager.permission.playerInGroup(player, (String)Next.getKey()))
				{
					line = line.replaceAll("%nextrank%", Next.getValue());  
				}
			}
			for(Entry<String, Integer> Cost : plugin.ranks.Price.entrySet())
			{
				if(plugin.vaultManager.permission.playerInGroup(player, (String)Cost.getKey()))
				{
					line = line.replaceAll("%cost%", String.valueOf(Cost.getValue()));  
				}
			}
			Double money = plugin.vaultManager.economy.getBalance(player);
			line = line.replaceAll("%balance%", String.valueOf(money));
			
			score = objective.getScore(line);
			score.setScore(pos);
		    pos--;
		}
	}
	//Aktualizacja scoreboards
    public void updateScoreboard(Player player) 
    {
    	player.setScoreboard(prepareScoreboard(player));
    }
    
}